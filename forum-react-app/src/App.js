import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { ApolloClient, ApolloProvider, InMemoryCache, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import ForumPage from './pages/ForumPage';
import LoginPage from './pages/LoginPage';
import { isLoggedIn } from "./services/useAuth";
import ContainerPage from "./pages/ContainerPage";

const httpLink = createHttpLink({
  uri: 'http://localhost:8000/graphql/',
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('token');

  if (isLoggedIn()) {
    return {
      headers: {
        ...headers,
        authorization: token ? `JWT ${token}` : '',
      }
    }
  }
  else {
    return {
        headers: {
        ...headers,
      }
    };
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/forum"></Redirect>
          </Route>
          <Route path="/login" component={LoginPage} />
          <Route path="/forum" component={ContainerPage} />
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
