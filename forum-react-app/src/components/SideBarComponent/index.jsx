import React from 'react';
import { useQuery, gql } from '@apollo/client';
import {
  BadgesContainer,
  Badge,
  BadgeIcon,
  BadgeLabel,
  Container,
  FirstSection,
  FollowersLabel,
  JoinedLabel,
  SecondSection,
  UserBadge,
  UserContainer,
  UserIcon,
  UserInfo,
  UserInfoContainer,
  UserName,
  UserTitle,
  UserTitleContainer,
  Sections,
  StatusLabel,
  TrendCategories,
  BottomContainer,
  TrendTitle,
  TrendTitleContainer,
  TrendIcon,
  TrendCategory,
  TrendCategoryTitle,
  TrendCategorySubtitle,
  RecentActivityContainer,
  RecentContainer,
  Recent,
  RecentIcon,
  RecentInfo,
  RecentComment,
  RecentTime,
  RecentUser,
  Footer,
  FooterLink,
  FooterSection
} from './styles';
import Icon from '@mdi/react';
import { mdiAccount, mdiCheckDecagram, mdiCog } from '@mdi/js';
import CondensedPostComponent from '../CondensedPostComponent';

const TOP_POSTS = gql`
  query {
    topPosts {
      id, title, createdDate, rating, upvotes, downvotes
    }
  }
`;

const SideBarComponent = () => {
  const { loading, error, data } = useQuery(TOP_POSTS);

  if (!data) {
    <p>Loading...</p>
  }

  return (
    <Container>
      <UserContainer>
        <UserInfoContainer>
          <UserIcon>
            <Icon path={mdiAccount} size="40px" />
          </UserIcon>
          <UserInfo>
            <UserTitleContainer>
              <UserTitle>Aaron Hall</UserTitle>
              <UserBadge>
                <Icon path={mdiCheckDecagram} size="20px" />
              </UserBadge>
            </UserTitleContainer>
            <UserName>@aaronhall</UserName>
          </UserInfo>
        </UserInfoContainer>
        <Sections>
          <FirstSection>
            <FollowersLabel>4.6M followers</FollowersLabel>
            <JoinedLabel>Joined June 2009</JoinedLabel>
          </FirstSection>
          <SecondSection>
            <BadgesContainer>
              <Badge>
                <BadgeIcon color="#FFD600" />
                <BadgeLabel>227</BadgeLabel>
              </Badge>
              <Badge>
                <BadgeIcon color="#8E8E8E" />
                <BadgeLabel>363</BadgeLabel>
              </Badge>
              <Badge>
                <BadgeIcon color="#B17245" />
                <BadgeLabel>409</BadgeLabel>
              </Badge>
            </BadgesContainer>
            <StatusLabel>Plus Membership</StatusLabel>
          </SecondSection>
        </Sections>
      </UserContainer>
      <BottomContainer>
        <TrendTitleContainer>
          <TrendTitle>Trends for you</TrendTitle>
          <TrendIcon>
            <Icon path={mdiCog} size="16px" />
          </TrendIcon>
        </TrendTitleContainer>
        <TrendCategories>
          <TrendCategory>
            <TrendCategoryTitle>Tech</TrendCategoryTitle>
            <TrendCategorySubtitle>20.8k Questions</TrendCategorySubtitle>
          </TrendCategory>
          <TrendCategory>
            <TrendCategoryTitle>AR</TrendCategoryTitle>
            <TrendCategorySubtitle>1.3k Questions</TrendCategorySubtitle>
          </TrendCategory>
          <TrendCategory>
            <TrendCategoryTitle>Cinema</TrendCategoryTitle>
            <TrendCategorySubtitle>65k Questions</TrendCategorySubtitle>
          </TrendCategory>
        </TrendCategories>
        <RecentActivityContainer>
          <TrendTitle>
            Recent Activity
          </TrendTitle>
          <RecentContainer>
            <Recent>
              <RecentIcon>
                <Icon path={mdiAccount} size="26px" />
              </RecentIcon>
              <RecentInfo>
                <RecentUser>@amyharris</RecentUser>
                <RecentComment>Marked as solved</RecentComment>
                <RecentTime>1 min ago</RecentTime>
              </RecentInfo>
            </Recent>
            <Recent>
              <RecentIcon>
                <Icon path={mdiAccount} size="26px" />
              </RecentIcon>
              <RecentInfo>
                <RecentUser>@sambrown</RecentUser>
                <RecentComment>Asked a question</RecentComment>
                <RecentTime>2hr ago</RecentTime>
              </RecentInfo>
            </Recent>
            <Recent>
              <RecentIcon>
                <Icon path={mdiAccount} size="26px" />
              </RecentIcon>
              <RecentInfo>
                <RecentUser>@robertdavis</RecentUser>
                <RecentComment>Answered a question</RecentComment>
                <RecentTime>yesterday</RecentTime>
              </RecentInfo>
            </Recent>
          </RecentContainer>
        </RecentActivityContainer>
        <Footer>
          <FooterSection>
            <FooterLink>About</FooterLink>
            <FooterLink>Privacy</FooterLink>
            <FooterLink>Terms</FooterLink>
            <FooterLink>Languages</FooterLink>
          </FooterSection>
          <FooterSection>
            <FooterLink>Contact</FooterLink>
            <FooterLink>Your Add Choices</FooterLink>
            <FooterLink>Careers</FooterLink>
            <FooterLink>Forum Inc. 2021</FooterLink>
          </FooterSection>
        </Footer>
      </BottomContainer>
    </Container>
  );
};

export default SideBarComponent;
