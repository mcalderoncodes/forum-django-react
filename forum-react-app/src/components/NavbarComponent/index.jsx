import React, { useEffect, useState } from 'react';
import Icon from '@mdi/react';
import { mdiAccount, mdiBell, mdiPlus } from '@mdi/js';
import { Link, useHistory } from 'react-router-dom';
import useAuth from '../../services/useAuth';
import { Dropdown } from 'react-bootstrap';
import { Button, Container, IconButton } from './styles';
import SearchComponent from '../SearchComponent';

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <li className="nav-item">
    <a className="btn"
      ref={ref}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      { children }
    </a>
  </li>
));

const NavbarComponent = () => {
  const history = useHistory();
  const auth = useAuth();
  const [user, setUser] = useState(null);
  
  useEffect(() => {
    if (auth.isLoggedIn()) {
      auth.getUser().then(u => setUser(u));
    }
  }, [auth]);

  const handleNewPost = () => {
    history.push('/forum/create');
  }

  const handleLogout = () => {
    auth.logOut();
    window.location.href = '/forum';
  }

  return (
    <Container>
      <SearchComponent />
      <Button>Ask Question</Button>
      <IconButton>
        <Icon path={mdiBell} size="23px" color="#7884F3" />
      </IconButton>
    </Container>
  );
};

export default NavbarComponent;
