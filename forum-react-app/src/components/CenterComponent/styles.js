import styled from 'styled-components';

export const Container = styled.div`
    flex-grow: 1;
    overflow-y: scroll;
    overflow-x: hidden;
    height: calc(100vh - 100px);

    ::-webkit-scrollbar {
        display: none;
    }
     
    ::-webkit-scrollbar-track {
        display: none;
    }
     
    ::-webkit-scrollbar-thumb {
        display: none;
    }
`;
