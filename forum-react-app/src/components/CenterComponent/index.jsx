import React from 'react';
import PostComponent from '../PostComponent';

import { Container } from './styles';

const CenterComponent = ({ posts }) => {
  if (!posts) {
    return <p>Loading...</p>;
  }

  return (
    <Container>
      {posts && posts.map(p => (
        <PostComponent key={p.id} data={p} />
      ))}
    </Container>
  );
};

export default CenterComponent;
