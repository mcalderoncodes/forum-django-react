import React from 'react';
import { useQuery, gql } from '@apollo/client';
import CenterComponent from '../../components/CenterComponent';
import NavbarComponent from '../../components/NavbarComponent';
import NavigationBarComponent from '../../components/NavigationBarComponent';
import SideBarComponent from '../../components/SideBarComponent';

import { CenterContainer, EndContainer, Wrapper } from './styles';

const FEED_POSTS = gql`
  query {
    feedPosts {
      id, title, category { id, name }, content, createdDate, upvotes, downvotes, creator { id, username }
    }
  }
`;

const ContainerPage = () => {
  const { data } = useQuery(FEED_POSTS);

  return (
    <Wrapper>
      <NavigationBarComponent />
      <CenterContainer>
        <NavbarComponent />
        <CenterComponent posts={data && data.feedPosts} /> 
      </CenterContainer>
      <EndContainer>
        <SideBarComponent />
      </EndContainer>
    </Wrapper>
  );
};

export default ContainerPage;
